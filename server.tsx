const fs = require('fs');
const bodyParser = require('body-parser');
const jsonServer = require('json-server');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const express = require('express');
const server = jsonServer.create()
const router = jsonServer.router('./db.json')
const userdb = JSON.parse(fs.readFileSync('./users.json', 'UTF-8'))
server.use(jsonServer.defaults());
server.use(bodyParser.urlencoded({extended: true}))
server.use(bodyParser.json())
const SECRET_KEY = '123456789'
const expiresIn = '1h'
const app = express();
app.use(cookieParser());

const access_token = jwt.sign({ userEmail: "eliranyihye@gmail.com"}, SECRET_KEY);


  
  function verifyToken(token){
    return  jwt.verify(token, SECRET_KEY, (err, decode) => decode !== undefined ?  decode : err)
  }
  
  function isAuthenticated({email, password}){
      console.log(email +" " + password)
    return userdb.users.findIndex(user => user.email === email && user.password === password) !== -1
  }
  server.post('/auth/login', (req, res) => {
      console.log()
    const {email, password} = req.body
    if (isAuthenticated({email, password}) === false) {
      const status = 401
      const message = 'Incorrect email or password'
      res.status(status).json({status, message})
      return
    }
    console.log("got Token")
    return res
    //saving the token as a cookie so it will not be stored on the client side/
    .cookie("access_token", access_token, {
      httpOnly: true,
      secure: false
    })
    .status(200).json({message: "Logged in :)"});
  })
  

  server.use(/^(?!\/auth).*$/,  (req, res, next) => {
    if (!req.headers.cookie) {
      const status = 401
      const message = 'Bad authorization header - You need to Log in'
      res.status(status).json({ status, message})
      return
    }
    try {
      const token = req.headers.cookie.replace("access_token=", "");
      const data = jwt.verify(token,SECRET_KEY);
      req.userEmail = data.userEmail;
      return next();
    } catch (err) {

      const status = 401
      const message = 'Error: access_token is not valid'
      res.status(status).json({status, message})
    }
  })
  server.use(router)

server.listen(3000, () => {
  console.log('Run Auth API Server')
  userdb.users.map((user) => console.log(user.email + " " + user.password));
  console.log(userdb);
})