
import React, { useState } from "react";
import i18next from "i18next";
import { DropdownButton, Dropdown } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import './index.css'
const Navbar = () => {
    const { t } = useTranslation();
    var allLanguages = [{

        language: "English",
        lan: "en"
    },
    {
        language: "Hebrew",
        lan: "he"
    }];
    var prefLanguage = localStorage.getItem('prefferedLanguage');
    allLanguages.unshift(
        allLanguages.splice(
            allLanguages.findIndex(
                elt => elt.lan === prefLanguage
            ),
            1)[0]
    )

    var prefLanguage = localStorage.getItem('prefferedLanguage');

    const [currentLanguage, setCurrentLanguage] = useState(prefLanguage);
    const [hasLanguageChanged, setHasLanguageChanged] = useState(false);
    const [isDropdown, setIsDropdown] = useState(false);


            const [languages, setLanguages] = useState(
                prefLanguage != null ?
                    allLanguages
        
                    :
                    [{
        
                        language: "English",
                        lan: "en"
                    },
                    {
                        language: "Hebrew",
                        lan: "he"
                    }]);
    const changeLanguage = (Lang:string, i:number) => {

        setHasLanguageChanged(true);
        i18next.changeLanguage(Lang);
        setCurrentLanguage(Lang);
        var lang = languages[i];
        var langs = languages;
        langs.unshift(lang);
        langs.splice(i + 1);
        //langs.shift(i+1);
        setLanguages([...langs]);
        localStorage.setItem('prefferedLanguage', lang.lan);
        setTimeout(() => {
            setHasLanguageChanged(false);

        }, 1000);


    }
    return(
        <>
        <div className="nav">
        <DropdownButton  variant={"dark"} className="dropdown" title={t('Change language')} onClick={() => setIsDropdown(true)} >
                                {languages.map((Lang, i) => (
                                    <Dropdown.Item className="dropdown-item" disabled={i == 0} key={i} active={i === 0 ? true : false} onClick={i === 0 ? undefined : () => {
                                        changeLanguage(Lang.lan, i);
                                    }}>{t(Lang.language)}</Dropdown.Item>
                                ),
                                )}
                            </DropdownButton>
        </div>
        </>
    )
}

export default Navbar;