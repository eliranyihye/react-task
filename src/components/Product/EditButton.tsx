import React, { useState } from "react";
import { Button, Modal, Form } from 'react-bootstrap';
import { useTranslation } from "react-i18next";

const EditButton = (props: { id: number, productsCounter: number, title: string, text: string }) => {
    const { t } = useTranslation();

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [summary, setSummary] = useState("");
    const [title, setTitle] = useState("");

    const editProduct = (id: number) => {

            fetch('http://localhost:3001/products/' + id,
                {
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    method: "PUT",
                    body: JSON.stringify({ "Title": title, "text": summary })
                })
                .then(function (res) { console.log(res) })
                .catch(function (res) { console.log(res) })
                handleClose();
    }
    return (
        <>
            <Button variant="success" size="sm"
            onClick={handleShow}
            >{t('product.edit')}
                <i className="fa fa-edit">

                </i>
            </Button>
            <Modal show={show} onHide={handleClose} size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>Add new product</Modal.Title>

                </Modal.Header>
                <Form>
                    <Form.Group controlId="Title">
                        <Form.Label>Product name</Form.Label>
                        <Form.Control type="text" placeholder={props.title}
                            value={title}
                            onChange={(e) => setTitle(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Product summary</Form.Label>
                        <Form.Control type="text" placeholder={props.text}
                            value={summary}
                            onChange={(e) => setSummary(e.target.value)} />
                    </Form.Group>


                </Form>


                <Modal.Body>Cancel to go back to the main page or press Edit product to edit and save the changes</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancel
                    </Button>
                    <Button  variant="primary" onClick={() => editProduct(props.id)}>
                        Edit Product
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default EditButton;