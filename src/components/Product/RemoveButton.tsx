import React, {useState} from "react";
import { Button } from 'react-bootstrap'; 
import { useTranslation } from "react-i18next";

const RemoveButton = (props: {id:number, productsCounter:number}) => {
    const { t } = useTranslation();
    var index:number = props.id;
    const removeProduct = (id:number, productsCounter:number) => {
        fetch('http://localhost:3001/products/' + (id),
            {
                headers: {
                    'Content-Type': 'application/json'
                },
                method: "DELETE",
            })
            .catch(function (res) { console.log(res) })
            for(var i =1; i <= productsCounter-id; i++) {
                var j:number = i;

                fetch('http://localhost:3001/products/'+(id+i),
                {
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    method: "GET",
                })
                .then(res =>{ 
                fetch('http://localhost:3001/products',
                {
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    method: "POST",
                    body: JSON.stringify({"title":"title", "text":"text", "id": id+i})

                })         
                .catch(response => { console.log(response) })
            })
            }
        }
    return(
    <Button
     variant="danger"
      size="sm"
      onClick={() => removeProduct(props.id, props.productsCounter)}
      >{t('product.remove')}  
        <i className="fa fa-trash">
        </i>

    </Button>
    )

}

export default RemoveButton;