import React from "react";
import EditButton from "./EditButton";
import RemoveButton from "./RemoveButton";
import './index.css';
import { useTranslation } from "react-i18next";

const Product = (props:{title:string, text:string, index:number, productsCounter:number}) => {
    const { t } = useTranslation();
    return(
    <div 
    className="product"
    key={props.index}
    >
        <h1>{props.title}</h1>
        <p>{props.text}</p>
        <div
        className="buttons">
        <RemoveButton id={props.index} productsCounter={props.productsCounter}/>
        <EditButton id={props.index} productsCounter={props.productsCounter} title={props.title} text={props.text}/>
        </div>
    </div>
    )
}

export default Product;