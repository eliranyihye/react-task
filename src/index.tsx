import React from 'react';
import './index.css';
import App from './app/App';
import { hydrate, render } from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import {I18nextProvider} from "react-i18next";
import i18next from "i18next";
import './i18n'
i18next.init({
  interpolation: { escapeValue: false },  // React already does escaping
});

const rootElement = document.getElementById('root')!

if (rootElement.hasChildNodes()) {
  hydrate(
    <I18nextProvider i18n={i18next}>
  <App />
  </I18nextProvider>
  , rootElement);
} else {
  render(
  <App />
  , rootElement);
}
