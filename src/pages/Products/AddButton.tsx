import React, { useState } from "react";
import { Button } from 'react-bootstrap';
import { Modal, Form } from "react-bootstrap";
import { useTranslation } from "react-i18next";

const AddButton = () => {
    const [show, setShow] = useState(false);
    const [title, setTitle] = useState("");
    const [summary, setSummary] = useState("");
    const { t } = useTranslation();

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const postProduct = () => {
        fetch('http://localhost:3001/products',
            {
                headers: {
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify({ "Title": title, "text": summary })
            })
            .then(function (res) { console.log(res) })
            .catch(function (res) { console.log(res) })
            handleClose();
    }
    return (
        <>
            <Button variant="primary" size="sm" className="button" onClick={handleShow}>
                {t('product.add')}
                <i className="fa fa-table">

                </i>
            </Button>
            <Modal show={show} onHide={handleClose} size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>Add new product</Modal.Title>

                </Modal.Header>
                <Form>
                    <Form.Group controlId="Title">
                        <Form.Label>Product name</Form.Label>
                        <Form.Control type="text" placeholder="Enter Product name"
                            value={title}
                            onChange={(e) => setTitle(e.target.value)} />
                    </Form.Group>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Product summary</Form.Label>
                        <Form.Control type="text" placeholder="Enter Product summary"
                            value={summary}
                            onChange={(e) => setSummary(e.target.value)} />
                    </Form.Group>


                </Form>


                <Modal.Body>Cancel to go back to the main page or press Add product to add the product</Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancel
                    </Button>
                    <Button disabled={title.length < 5 || summary.length < 20} variant="primary" onClick={() => postProduct()}>
                        Add Product
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default AddButton;