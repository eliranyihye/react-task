import React, { useEffect, useState } from "react";
import Product from "../../components/Product";
import AddButton from "./AddButton";
import './index.css';
const Products = () => {
    const [products, setProducts] = useState([] as any[]);
    const [productsIndexed, setProductsIndexed] = useState([] as any[]);

    const getProducts = () => {

        fetch('http://localhost:3001/products')
            .then((res) => res.json())
            .then((json) => setProducts((json)))
    }
    useEffect(() => {
        getProducts();

    }, [products])

    return (
        <>
        <div className="products">
            <AddButton />
                {products.map((product, i) => <Product
                    key={i}
                    title={product['Title']}
                    text={product['text']}
                    index={product['id']}
                    productsCounter={products.length}
                />)}
            </div>

        </>
    )
};

export default Products;