import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { Subject } from 'rxjs';
import { Helmet } from "react-helmet";
import { Button } from 'react-bootstrap'; 

const Login = () => {
    const { t, i18n } = useTranslation();
    const [email, setEmail] = useState("");
    const [pass, setPass] = useState("");
    const loginPost = () => {
        fetch('http://localhost:3001/auth/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({"email": email, "password": pass})
        })
            .then(res => {if(res.status ===200) {window.location.replace('/')}})
    }
    return (
        <>
            <div className="container">
                <Helmet>
                    <title>Login | Task</title>
                </Helmet>
                <fieldset>
                    <form className={"contact__form"} method="post">
                        <header className="contact_header">
                            <h1>
                                {t('login.login')}
                            </h1>
                        </header>
                        <input
                            className="email__input"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            placeholder={t('login.email')}
                        >

                        </input>
                        <input
                        type={"password"}
                            className="password__input"
                            value={pass}
                            onChange={(e) => setPass(e.target.value)}
                            placeholder={t('login.password')}>
                        </input>
                        <Button
                            disabled={!email || !pass}
                            variant='light'
                            className="Submit__button"
                            onClick={() => loginPost()}
                        >
                            {t('login.login')}
                                                    </Button>
                    </form>
                </fieldset>
            </div>
        </>
    )

}

export default Login;