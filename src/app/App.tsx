import React, { Fragment } from 'react';
import './App.css';
import { lazy } from 'react';
import { BrowserRouter, Switch, Route, useLocation, Redirect } from 'react-router-dom';
import { useState } from 'react';
import { Suspense } from 'react';
import Navbar from '../components/Navbar'
const loginPage = lazy(() => import('../pages/login'));
const productsPage = lazy(() => import('../pages/Products/index'));

const App = () => {

  return (
    <BrowserRouter>
    
      <AppRoutes />

    </BrowserRouter>
  )

}

const AppRoutes = () => {
  const [currentLanguage, setCurrentLanguage] = useState(localStorage.getItem('prefferedLanguage'));
  //const [token, setToken] = useState
  const isTokenValid = () => {
    var resStatus;
    fetch('http://localhost:3001/products',
    {
      headers: {
          'Content-Type': 'application/json'
      },
      method: "GET",
  })
  .then(res => {resStatus = res.status});

  return resStatus === 200 || 304;
  }
  return (
    <Fragment>
      <Navbar />
      <div className="app_page">
        <Suspense fallback={<Fragment />}>
          <Switch>
            {
            isTokenValid() &&
            <Route exact path="/" component={productsPage} />
}
            <Route path="/login" component={loginPage} />
            <Redirect to="/login" />

          </Switch>
        </Suspense>
      </div>
    </Fragment>
  )
}


export default App;
